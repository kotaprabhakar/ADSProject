#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <cstring>
#include <map>
#include <cmath>
#include <queue>
#include <sstream>

using namespace std;

class Node;

/**
 * CLASS: NodeElement
 * DESCRIPTION: Represents three types of elements inside a B+ Tree Node
 *      1. First element of a B+ Tree Node. It has undefined key, undefined values(data) and a defined pointer to a B+ Tree Internal or Data Node
 *      2. Normal element of a B+ Tree Internal Node. It has a key, undefined values(data) and a defined pointer to a B+ Tree Internal or Data Node
 *      3. Normal element of a B+ Tree Data Node. It has a key, defined values(data) and an undefined pointer
 * PRIVATE MEMBERS: 
 *      1. key: stores key of the element
 *      2. values: stores values associated with key if present in data node. Otherwise it's NULL
 *      3. node: pointer to another node of the B+ Tree
 * METHODS: Described in definitions in BPlusTree.cpp
*/
        class NodeElement{
        private:
            double key;
            vector<string> values;
            Node *node;
        public:
            NodeElement(double key, vector<string> values);
            NodeElement(Node *node);
            NodeElement(double key, Node *node);
            double get_key();
            void set_key(double key);
            Node *get_node();
            void set_node(Node *node);
            vector<string> get_values();
            void add_value(string new_value);
        };



/**
 * CLASS: Node
 * DESCRIPTION: Represents a node of a B+ Tree Node. The node can be a data node or an internal node
 * PRIVATE MEMBERS:
 *      1. elements: vector of NodeElement objects each of which stores key, values and/or a pointer to another node
 *      2. parent: pointer to parent B+ Tree node
 *      3. left_node: only for data nodes. Pointer to left data node in doubly linked list
 *      4. right_node: only for data nodes. Pointer to right data node in doubly linked list
 *      5. m: order of B+ Tree
 *      6. n: number of elements in the node currently 
 * METHODS: Described in definitions in BPlusTree.cpp
*/
        class Node{
        private:
            vector<NodeElement> elements;
            Node *parent;
            Node *left_node;
            Node *right_node;
            int m;
            int n;
            void insert(Node *node, Node *&root);
        public:
            Node();
            Node(vector<NodeElement> elements);
            void destruct();
            int search_index(double key);
            vector<NodeElement> get_elements();
            void set_order(int m);
            int get_order();
            int get_count();
            void insert(double key, string value, Node *&root);
            vector<string> search(double key);
            vector<string> *search(double key_from, double key_to);
            void set_left_node(Node *left_node);
            void set_right_node(Node *right_node);
            Node *get_left_node();
            Node *get_right_node();
            void set_parent(Node *parent);
        };



/**
 * CLASS: BPlusTree
 * DESCRIPTION: Class to access memory resident B+ Tree
 * PRIVATE MEMBERS:
 *      1. root: pointer to root node of B+ Tree
 *      2. m: order of B+ Tree
 * METHODS: Described in definitions in BPlusTree.cpp
*/
        class BPlusTree{   
        private:
            Node *root;
            int m;
        public:
            BPlusTree(int m);
            void initialize(int m);
            void insert(double key, string value);
            Node *get_root();
            vector<string> search(double key);
            vector<string> *search(double key_from, double key_to);
        };

