#include "BPlusTree.h"

/**
 * CLASS: NodeElement
 * DESCRIPTION: Represents three types of elements inside a B+ Tree Node
 *      1. First element of a B+ Tree Node. It has undefined key, undefined values(data) and a defined pointer to a B+ Tree Internal or Data Node
 *      2. Normal element of a B+ Tree Internal Node. It has a key, undefined values(data) and a defined pointer to a B+ Tree Internal or Data Node
 *      3. Normal element of a B+ Tree Data Node. It has a key, defined values(data) and an undefined pointer
 * PRIVATE MEMBERS: 
 *      1. key: stores key of the element
 *      2. values: stores values associated with key if present in data node. Otherwise it's NULL
 *      3. node: pointer to another node of the B+ Tree
*/

        /**
         * DESCRIPTION:
         *      1. Constructor for class NodeElement
         *      2. Used for NodeElement objects inside Data Nodes of B+ Tree
         * PARAMETERS:
         *      1. key: key of dictionary values stored in this object
         *      2. values: values associated with given key 
        */
        NodeElement::NodeElement(double key, vector<string> values)
        {
            this->key = key;
            this->values = values;
            this->node = NULL;
        }

        /**
         * DESCRIPTION:
         *      1. Constructor for class NodeElement
         *      2. Used for NodeElement object for the first element of any Node (which has a single pointer, undefined values and key)
         * PARAMETERS:
         *      1. node: pointer to point to left most child of the present node
        */
        NodeElement::NodeElement(Node *node)
        {
            // This key is not validated everytime this node element is accessed since it's the first element of the node
            this->key = -1; 
            this->node = node;
        }

        /**
         * DESCRIPTION:
         *      1. Constructor for class NodeElement
         *      2. Used for NodeElement object for a normal element of internal nodes
         * PARAMETERS:
         *      1. key: key to be stored in this object of internal node
         *      2. node: pointer to child to the right of the key of this node element inside present node
        */
        NodeElement::NodeElement(double key, Node *node)
        {
            this->key = key;
            this->node = node;
        }

        /**
         * DESCRIPTION:
         *      Getter for key of this NodeElement (Doesn't exist for first NodeElement object of a node)
         * RETURN TYPE:
         *      double (datatype of key)
        */
        double NodeElement::get_key()
        {
            return this->key;
        }

        /**
         * DESCRIPTION: 
         *      Setter for key of this NodeElement (Doesn't exist for first NodeElement object of a node)
         * PARAMETERS:
         *      key: value of key to be set in this NodeElement object
         * RETURN TYPE:
         *      void
        */
        void NodeElement::set_key(double key)
        {
            this->key = key;
        }

        /**
         * DESCRIPTION: 
         *      Getter for child node pointed to by this NodeElement
         * RETURN TYPE:
         *      Node *: pointer to the child node
        */
        Node *NodeElement::get_node()
        {
            return this->node;
        }

        /**
         * DESCRIPTION: 
         *      Setter for child node pointed to by this NodeElement
         * PARAMETERS:
         *      key: value of key to be set in this NodeElement object
         * RETURN TYPE:
         *      void
        */
        void NodeElement::set_node(Node *node)
        {
            this->node = node;
        }

        /**
         * DESCRIPTION:
         *      Getter for values associated with this object
         * RETURN TYPE:
         *      String vector of all values
        */
        vector<string> NodeElement::get_values()
        {
            return this->values;
        }

        /**
         * DESCRIPTION: 
         *      Setter for child node pointed to by this NodeElement
         * PARAMETERS:
         *      key: value of key to be set in this NodeElement object
         * RETURN TYPE:
         *      void
        */
        void NodeElement::add_value(string new_value)
        {
            //insert new_value into this node element at the back using push_pack function of vector values
            this->values.push_back(new_value);
        }

/**
 * CLASS: Node
 * DESCRIPTION: Represents a node of a B+ Tree Node. The node can be a data node or an internal node
 * PRIVATE MEMBERS:
 *      1. elements: vector of NodeElement objects each of which stores key, values and/or a pointer to another node
 *      2. parent: pointer to parent B+ Tree node
 *      3. left_node: only for data nodes. Pointer to left data node in doubly linked list
 *      4. right_node: only for data nodes. Pointer to right data node in doubly linked list
 *      5. m: order of B+ Tree
 *      6. n: number of elements in the node currently 
*/

        /**
         * DESCRIPTION: 
         *      Constructor for Node class. Initializing all class variables.
        */
        Node::Node()
        {
            vector<NodeElement> elements;
            elements.push_back(*(new NodeElement((Node *)NULL)));
            this->elements = elements;
            this->parent = NULL;
            this->right_node = NULL;
            this->left_node = NULL;
            n = 0;
        }

        /**
         * DESCRIPTION: 
         *      Constructor for Node class. Initializes node with given elements
         * PARAMETERS:
         *      elements: vector of elements to put into this node
        */
        Node::Node(vector<NodeElement> elements)
        {
            this->elements = elements;
            n = elements.size() - 1;
            this->parent = NULL;
            this->right_node = NULL;
            this->left_node = NULL;
        }

        /**
         * DESCRIPTION: 
         *      Recursive destructor to destruct all node attached to this node and then to itself
         * RETURN TYPE:
         *      void
        */
        void Node::destruct()
        {
            int i = 0;
            // Call destructor of all child nodes to this node first
            for(i = 0; i < elements.size(); i++){
                if(elements.at(i).get_node() != NULL)
                    elements.at(i).get_node()->destruct();
            }
            //Delete the vector of NodeElement objects inside this node
            for(i = 0; i < elements.size(); i++)
                vector<NodeElement>().swap(elements);
            //Finally delete this node
            delete this;
        }

        /**
         * DESCRIPTION:
         *      Getter to get vector of elements in this node
         * RETURN TYPE:
         *      vector of NodeElement objects
        */
        vector<NodeElement> Node::get_elements()
        {
            return elements;
        }

        /**
         * DESCRIPTION: 
         *      Setter to set order of B+ Tree to each node
         * PARAMETERS:
         *      m: order to set
         * RETURN TYPE:
         *      void
        */
        void Node::set_order(int m)
        {
            this->m = m;
        }

        /**
         * DESCRIPTION:
         *      GETTER to get order from node
         * RETURN TYPE":
         *      int
        */
        int Node::get_order()
        {
            return m;
        }

        /**
         * DESCRIPTION:
         *      GETTER to get number of elements in this node
         * RETURN TYPE:
         *      int
        */
        int Node::get_count()
        {
            return n;
        }

        /**
         * DESCRIPTION: 
         *      1. Search for element with certain key
         *      2. Returns index of element in vector<NodeElement> elements which has the same key as argument to function, or the index of the element after which an element with the argument key can be inserted
         * PARAMETERS:
         *      key: key to be searched in the node
         * RETURN TYPE:
         *      int
        */
        int Node::search_index(double key)
        {
            //Searching using binary search from the end to check if any element's key is less than or equal to key
            int first = 1, last = get_count(), middle = 0, res;
                    
                while (last >= first) 
                {
                    middle = ceil((first + last) / 2.0);

                    if (elements.at(middle).get_key() > key) 
                    {
                        last = middle - 1;
                    }
                    else if (elements.at(middle).get_key() < key) 
                    {
                        first = middle + 1;
                    }
                    else 
                    {
                        res = middle;
                        break;
                    }
            }
            if(elements.at(middle).get_key() > key)
                res = first - 1;
            else if(elements.at(middle).get_key() < key)
                res = last;
                
            return res;
        }

        /**
         * DESCRIPTION: 
         *      1. Inserts a new element with given key and value into this data node. Updates root of B+ Tree if required
         *      2. Updates already existing element's values if key already exists
         * PARAMETERS:
         *      1. key: key of element to be inserted
         *      2. value: value of that element
         *      3. root: root of B+tree which has to be updated in case of split
         * RETURN TYPE:
         *      void
        */
        void Node::insert(double key, string value, Node *&root)
        {
            vector<string> values_new;
            values_new.push_back(value);
            NodeElement *element = new NodeElement(key, values_new);
            
            //If there's no element in this node, insert left pointer, then new element which also has the right pointer
            if (n == 0)
            {
                elements.push_back(*element);
                n++;
            }
            else
            {
                //Get index at which the node with this key can be inserted
                int index = search_index(key);
                
                //If key already exists in node, add value to values vectors associated with this key
                if (elements.at(index).get_key() == key)
                {
                    elements.at(index).add_value(value);
                    delete element;
                }
                else
                {
                    //Create new NodeElement and insert at correct position in elements vector of this Node
                    elements.insert(elements.begin() + index + 1, *element);
                    //Increment the number of node elements in this node
                    n++;
                    //If Node becomes overfull, split it
                    if (n == m)
                    {
                        //Index at which node is to be split
                        int split_index = n / 2 + 1;
                        
                        //Second half of the node is stored in elements1
                        vector<NodeElement> elements1;
                        elements1.push_back(*(new NodeElement((Node *)NULL)));
                        elements1.reserve(elements1.size() + elements.size() - elements.size() / 2);
                        elements1.insert(elements1.end(), elements.begin() + split_index, elements.end());
                        
                        //Resize elements vector of this node
                        elements.assign(elements.begin(), elements.begin() + split_index);
                        n = elements.size() - 1;
                        
                        //Store elements1 in new data node
                        Node *data_node = new Node(elements1);
                        data_node->set_order(this->get_order());
                        
                        //Arrange doubly linked list left and right pointers of all data nodes involved
                        data_node->right_node = this->right_node;
                        if (data_node->right_node != NULL)
                            data_node->right_node->left_node = data_node;
                        data_node->left_node = this;
                        this->right_node = data_node;
                        
                        //Create node for internal node which is to be inserted in parent of this node
                        vector<NodeElement> internal_node_elements;
                        NodeElement *first_pointer_element = new NodeElement((Node *)NULL);
                        NodeElement *least_data_element = new NodeElement(elements1.at(1).get_key(), data_node);
                        internal_node_elements.push_back(*first_pointer_element);
                        internal_node_elements.push_back(*least_data_element);
                        Node *internal_node = new Node(internal_node_elements);
                        internal_node->set_order(this->get_order());
                        data_node->parent = internal_node;
                        internal_node->get_elements().at(1).set_node(data_node);
                        
                        //If parent of this node is NULL, internal_node becomes root
                        if (this->parent != NULL)
                        {
                            (this->parent)->insert(internal_node, root);
                        }
                        //If parent of this node is not NULL, internal_node is inserted into the parent
                        else
                        {
                            internal_node->elements.at(0).set_node(this);
                            this->parent = internal_node;
                            root = internal_node;
                        }
                    }
                }
            }
            
        }

        /**
         * DESCRIPTION:
         *      Setter to set parent pointer of current node
         * PARAMETERS:
         *      parent: pointer to node which is to be parent of current node
         * RETURN TYPE:
         *      void
        */
        void Node::set_parent(Node *parent)
        {
            this->parent = parent;
        }

        /**
         * DESRIPTION:
         *      Merges a new internal node with given key and value into this internal node after split. Updates root of B+ Tree if required
         * PARAMETERS:
         *      1. node: pointer to new internal node that is to be inserted
         *      2. root: root of B+ tree which might have to be updated
         * RETURN TYPE:
         *      void
        */
        void Node::insert(Node *node, Node *&root)
        {
            double key = node->get_elements().at(1).get_key();
            //Obtain index at which new internal node after split is to be inserted
            int index = search_index(key), i = 0;
            
            //Insert the node's node element containing the key of internal node after split
            elements.insert(elements.begin() + index + 1, node->get_elements().at(1));
            node->get_elements().at(1).get_node()->set_parent(this);
            n++;
            
            //This node might become overfull. So split
            if (n == m)
            {
                //Middle element to be extracted which is put in an internal node with right child as the newly split node
                int middle_element_index = ceil(n / 2.0);
                
                //Extract middle element
                double key = elements.at(middle_element_index).get_key();
                NodeElement node_element = elements.at(middle_element_index);
                
                //elements1 contains second part of this node after middle element. It will be put into newly split node
                vector<NodeElement> elements1;
                elements1.push_back(*(new NodeElement(node_element.get_node())));
                elements1.reserve(elements1.size() + elements.size() - middle_element_index - 1);
                elements1.insert(elements1.end(), elements.begin() + middle_element_index + 1, elements.end());
                
                //Create newly split node and put elements1 into it
                Node *split_node = new Node(elements1);
                split_node->set_order(this->get_order());
                node_element.get_node()->set_parent(split_node);
                
                //Assign parent pointers of child nodes of elements in newly split node to the newly split node
                for (i = middle_element_index + 1; i < elements.size(); i++)
                {
                    elements.at(i).get_node()->set_parent(split_node);
                }
                
                //Resize elements of this node to contain first part before middle element
                elements.assign(elements.begin(), elements.begin() + middle_element_index);
                n = elements.size() - 1;
                
                //Create an internal node to insert into parent of this node
                vector<NodeElement> upper_node_elements;
                NodeElement *first_pointer_element = new NodeElement((Node *)NULL);
                NodeElement *middle_key_element = new NodeElement(key, split_node);
                upper_node_elements.push_back(*first_pointer_element);
                upper_node_elements.push_back(*middle_key_element);
                Node *upper_node = new Node(upper_node_elements);
                upper_node->set_order(this->get_order());
                split_node->parent = upper_node;
                
                //If parent of this node is not NULL, insert new internal node upper_node into parent
                if (this->parent != NULL)
                {
                    (this->parent)->insert(upper_node, root);
                }
                //If parent of this node is NULL, make new internal node upper_node as the new root and assign present node's parent to the new root
                else
                {
                    upper_node->elements.at(0).set_node(this);
                    this->parent = upper_node;
                    root = upper_node;
                }
            }
        }

        /**
         * DESCRIPTION:
         *      Search for element with given argument key in this data node. Returns vector of string values associated with the key
         * PARAMETERS:
         *      key: key to search for in node
         * RETURN TYPE:
         *      vector of strings
        */
        vector<string> Node::search(double key)
        {
            //Search for key in given data node
            int index = search_index(key);
            
            //If key is found, return values associated with the key
            if (elements.at(index).get_key() == key)
            {
                return elements.at(index).get_values();
            }
            //Else return empty
            else
                return vector<string>();
        }

        /**
         * DESCRIPTION:
         *      Range Search for elements with keys in between range key_from and key_to
         * PARAMETERS:
         *      1. key_from: start key of range to be searched in
         *      2. key_to: end key of range to be searched in
         * RETURN TYPE:
         *      vector of strings with values
        */
        vector<string> *Node::search(double key_from, double key_to)
        {
            int index = search_index(key_from), i, j;
            ostringstream str_double;
            string pair = "";
            vector<NodeElement>::iterator iter;
            vector<string>::iterator iter1;
            vector<string> *result = new vector<string>();
            Node *temp = this;
            
            //If given key_from doesn't exist in B+ tree start range search from index next to returned index
            if (elements.at(index).get_key() < key_from)
                index++;
            
            do
            {
                //Loop around elements of node from returned index above till key_to is reached or end of node. If end of node is reached, start same loop in right node of this node till key_to is reached
                for (i = index; i < temp->get_elements().size() && temp->get_elements().at(i).get_key() <= key_to; i++)
                {
                    //Loop through values of element with key at index i of this node and add them in required format to result string vector which is to be returned
                    for (j = 0; j < temp->get_elements().at(i).get_values().size(); j++)
                    {
                        str_double << temp->get_elements().at(i).get_key();
                        pair = pair + "(" + str_double.str() + "," + temp->get_elements().at(i).get_values().at(j) + ")";
                        result->push_back(pair);
                        pair = "";
                        str_double.clear();
                        str_double.str(std::string());
                    }
                }
                if (i == temp->get_elements().size())
                {
                    temp = temp->get_right_node();
                    index = 1;
                }
                //When key_to is reached or end of doubly linked list is reached
                else
                    temp = NULL;
            } while (temp != NULL);
            
            return result;
        }

        /**
         * DESCRIPTION:
         *      Setter to set pointer to left node of this data node (applies only for data nodes) in doubly linked list
         * PARAMETERS:
         *      left_node: pointer to left node of this data node
         * RETURN TYPE:
         *      void
        */
        void Node::set_left_node(Node *left_node)
        {
            this->left_node = left_node;
        }

        /**
         * DESCRIPTION:
         *      Setter to set pointer to right node of this data node (applies only for data nodes) in doubly linked list
         * PARAMETERS:
         *      right_node: pointer to right node of this data node
         * RETURN TYPE:
         *      void
        */
        void Node::set_right_node(Node *right_node)
        {
            this->right_node = right_node;
        }

        /**
         * DESCRIPTION:
         *      Getter to get pointer to left node of this data node (applies only for data nodes) in doubly linked list
         * RETURN TYPE:
         *      pointer to Node
        */
        Node *Node::get_left_node()
        {
            return this->left_node;
        }

        /**
         * DESCRIPTION:
         *      Getter to get pointer to right node of this data node (applies only for data nodes) in doubly linked list
         * RETURN TYPE:
         *      pointer to Node
        */
        Node *Node::get_right_node()
        {
            return this->right_node;
        }

/**
 * CLASS: BPlusTree
 * DESCRIPTION: Class to access memory resident B+ Tree
 * PRIVATE MEMBERS:
 *      1. root: pointer to root node of B+ Tree
 *      2. m: order of B+ Tree
*/

        /**
         * DESCRIPTION:
         *      Constructor for B+ Tree. Initializes B+ tree with order m 
         * PARAMETERS:
         *      m: Order of B+ tree
        */
        BPlusTree::BPlusTree(int m)
        {
            initialize(m);
        }

        /**
         * DESCRIPTION:
         *      Initialize B+ tree with order m and root with NULL
         * PARAMETERS:
         *      m: order of B+ tree
         * RETURN TYPE:
         *      void
        */
        void BPlusTree::initialize(int m)
        {
            this->m = m;
            root = NULL;
        }

        /**
         * DESCRIPTION:
         *      Insert a node with given key and value into this B+ tree
         * PARAMETERS:
         *      1. key: key of new element pair to be added
         *      2. value: value of new element pair to be added
         * RETURN TYPE:
         *      void
        */
        void BPlusTree::insert(double key, string value)
        {
            //If root is NULL, create new data node
            if (root == NULL)
            {
                root = new Node();
                root->set_order(m);
                root->insert(key, value, root);
            }
            //Else search through B+ tree starting from root to reach a data node where given key can be inserted
            else
            {
                int index;
                
                Node *temp = root;
                index = temp->search_index(key);
                while (temp->get_elements().at(index).get_node() != NULL)
                {
                    temp = temp->get_elements().at(index).get_node();
                    index = temp->search_index(key);
                }
                //After reaching data node
                temp->insert(key, value, root);
            }
        }

        /**
         * DESCRIPTION:
         *      Search for given key in this B+ tree. Returns all values associated with this key
         * PARAMETERS:
         *      key: key to be searched
         * RETURN TYPE:
         *      string vector of values to be returned
        */
        vector<string> BPlusTree::search(double key)
        {
            int index;
            Node *temp = root;

            //Get index of element in root whose right subtree can contain given key
            index = temp->search_index(key);
            while (temp->get_elements().at(index).get_node() != NULL)
            {
                temp = temp->get_elements().at(index).get_node();
                index = temp->search_index(key);
            }

            //After reaching a data node
            return temp->search(key);
        }

        /**
         * DESCRIPTION:
         *      Search for values with keys in given range in this B+ tree. Returns all values associated with this keys withing this range
         * PARAMETERS:
         *      1. key_from: start key of range
         *      2. key_to: end key of range
         * RETURN TYPE:
         *      string vector of key value pairs to be returned
        */
        vector<string> *BPlusTree::search(double key_from, double key_to)
        {
            int index;
            Node *temp = root;
            //Get index of element in root whose right subtree can contain given key_from
            index = temp->search_index(key_from);
            while (temp->get_elements().at(index).get_node() != NULL)
            {
                
                temp = temp->get_elements().at(index).get_node();
                index = temp->search_index(key_from);
            }
            
            //After reaching data node
            vector<string> *results = temp->search(key_from, key_to);
            
            return results;
        }

        /**
         * DESCRIPTION:
         *      Getter to get root of B+ tree
         * RETURN TYPE:
         *      Node pointer
        */
        Node *BPlusTree::get_root()
        {
            return this->root;
        }

