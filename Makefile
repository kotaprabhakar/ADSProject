all: treesearch

treesearch: treesearch.o BPlusTree.o
	g++ treesearch.o BPlusTree.o -o treesearch

treesearch.o: treesearch.cpp
	g++ -c treesearch.cpp

BPlusTree.o: BPlusTree.cpp
	g++ -c BPlusTree.cpp

clean:
	rm -rf *o treesearch
