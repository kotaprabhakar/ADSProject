#include "BPlusTree.h"
#include <fstream>
using namespace std;

/**
 * DESCRIPTION:
 *      Main function to instantiate B+ tree and handle file input/output
 * PARAMETERS:
 *      1. argc: Number of arguments in call to ouput executable file of this program
 *      2. argv: Array of character strings of arguments in call to ouput executable file of this program
*/

        int main(int argc, char **argv)
        {
            const char *file_name = argv[1];
            
            //Stores each line in input file as a string
            string line_input;
            
            //Open input file given as argument to main function
            ifstream input_file(file_name);
            if (input_file.is_open())
            {
                getline(input_file, line_input);
                int x = atoi(line_input.c_str());
                
                //Open output file to output results of search queries
                ofstream output_file;
                output_file.open("output_file.txt");
                
                //Instantiate a new B+ tree
                BPlusTree *b_plus_tree = new BPlusTree(x);
                
                //Loop through all lines of input file
                while (getline(input_file, line_input))
                {
                    //If line contains insert query
                    if (!strncmp(line_input.c_str(), "Insert", 6))
                    {
                        string args = line_input.substr(7, line_input.size() - 8), first_arg, arg2;
                        first_arg = strtok(strdup(args.c_str()), ",");
                        arg2 = strtok(NULL, ",");
                        double arg1 = atof(first_arg.c_str());
                        //arg1 contains key and arg2 contains value of pair to be inserted
                        b_plus_tree->insert(arg1, arg2);
                    }
                    //If line contains search query
                    else if (!strncmp(line_input.c_str(), "Search", 6))
                    {
                        string args = line_input.substr(7, line_input.size() - 8), first_arg, second_arg;
                        char *temp_string;
                        
                        first_arg = strtok(strdup(args.c_str()), ",");
                        if ((temp_string = strtok(NULL, ",")) != NULL)
                            second_arg = temp_string;
                        
                        //If search is for single key
                        if (second_arg.empty())
                        {
                            //first_arg contains key to be searched in B+ tree
                            vector<string> results = b_plus_tree->search(atof(first_arg.c_str()));
                            if (!results.empty())
                            {
                                output_file << results.at(0);
                                for (int i = 1; i < results.size(); i++)
                                {
                                    output_file << ", " << results.at(i);
                                }
                            }
                            else
                                output_file << "Null";
                            output_file << endl;
                        }
                        //If search is a range search
                        else
                        {
                            //first_arg contains starting key of range search, second_arg contains ending key of range search
                            vector<string> *m = b_plus_tree->search(atof(first_arg.c_str()), atof(second_arg.c_str()));
                            if (!m->empty())
                            {
                                vector<string>::iterator i = m->begin();
                                output_file << m->at(0);
                                i++;
                                while (i != m->end())
                                {
                                    output_file << "," << *i;
                                    i++;
                                }
                            }
                            else
                            {
                                output_file << "Null";
                            }
                            output_file << endl;
                        }
                    }
                }
                output_file.close();
                
                //destruct b_plus_tree recursively to avoid memory leak
                b_plus_tree->get_root()->destruct();
                delete b_plus_tree;
                
                input_file.close();
            }
            
            return 0;
        }

